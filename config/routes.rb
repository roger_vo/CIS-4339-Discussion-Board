Rails.application.routes.draw do
  get 'docs/index', to: 'docs#index'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users, controllers: {
      sessions: 'users/sessions',
      passwords: 'users/passwords',
      registrations: 'users/registrations'
  }
  root to: "home#index"
  get :search, controller: :home

  resources :users do
    collection {post :import}
  end

  resources :channels do
    resources :posts do
      get 'tag/:tag', to: 'posts#index', as: :tag
    end
  end

  resources :posts do
    resources :comments
  end

  namespace :charts do
    get "index"
    get "new-posts"
    get "new-posts-per-channel"
    get "posts-day-user"
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
