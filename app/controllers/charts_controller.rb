class ChartsController < ApplicationController
  def new_posts
    render json: Post.group_by_day(:created_at).count
  end

  def new_posts_per_channel
    channel = Channel.new
    render json: Post.group_by_day(:created_at).group(:channel_id).count, label: channel.title
  end

  def posts_day_user
    render json: Post.group_by_day(:created_at).group(:user_id).count
  end

  def index
    # use ransack to create search object
    @q = Post.ransack(params[:q])
    @posts = @q.result

    respond_to do |format|
      format.html
    end
  end


end