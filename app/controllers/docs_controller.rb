class DocsController < ApplicationController
  def index
    render file: 'docs/index.html', layout: false
  end
end