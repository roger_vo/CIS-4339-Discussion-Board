class HomeController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index], raise: false
  # before_action :force_json, only: :search
  def index
    @channel = Channel.where(title: 'Announcement').take
    @posts = @channel.posts.order('created_at DESC').limit(5) #posts sorted descending order by created date; take last 5
  end

  def search
    @posts = Post.ransack(post_title_or_body_cont: params[:q]).result(distinct: true)

    respond_to do |format|
        format.html{}
        format.json{
          @posts = @posts.limit(5)
        }
    end
  end
  # private
  # def force_json
  #   request.format = :json
  # end
end
