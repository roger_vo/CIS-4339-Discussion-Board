class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  # GET /posts
  # GET /posts.json
  def index
    @channel = Channel.find(params[:channel_id])
    if params[:tag]
      @posts = @channel.posts.order(:sort).tagged_with(params[:tag])
      @posts.comments
    else
      @posts = @channel.posts.order(:sort).all
    end
    @comment = Comment.new
    @channel.posts.build
    @post = Post.new
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    channel = Channel.find (params[:channel_id])
    @post = channel.posts.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json  { render json: @post }
    end
  end

  # GET /posts/new
  def new
    @channel = Channel.find(params[:channel_id])
    #2nd you build a new one
    @post = @channel.posts.build
    @post.user = current_user
  end

  # GET /posts/1/edit
  def edit
    channel = Channel.find(params[:channel_id])
    #2nd you retrieve the comment thanks to params[:id]
    @comment = channel.posts.find(params[:id])
  end

  # POST /posts/:post_id/comments
  # POST /posts/:post_id/comments.xml
  def create
    channel = Channel.find(params[:channel_id])
    @post = channel.posts.create(post_params)
    @post.user = current_user
    respond_to do |format|
      if @post.save
        format.html { redirect_to channel_posts_path(channel, @post)}
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    channel = Channel.find(params[:channel_id])
    #2nd you retrieve the comment thanks to params[:id]
    @post = channel.posts.find(params[:id])
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to ([@post.channel, @post]) }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    channel = Channel.find(params[:channel_id])
    @post = channel.posts.find(params[:id])
    @post.destroy
    respond_to do |format|
      format.html { redirect_to channel_posts_path(channel,@post) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:post_title, :body, :user_id, :channel_id, :isDeleted, :isPrivate, :isEdited, :tag_list)
    end
end
