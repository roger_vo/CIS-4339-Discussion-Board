json.extract! user, :id, :user_name, :password, :employee_no, :email, :phone_number, :address, :city, :state, :zip_code, :user_status_id, :created_at, :updated_at
json.url user_url(user, format: :json)
