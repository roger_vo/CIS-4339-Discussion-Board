json.posts do
  json.array!(@posts) do |post|
    json.name post.post_title
    json.url channel_posts_path(post.channel, post)
  end
end

