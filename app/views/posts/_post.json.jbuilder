json.extract! post, :id, :post_title, :body, :user_id, :channel_id, :isDeleted, :isPrivate, :post_tag_id, :isEdited, :created_at, :updated_at
json.url post_url(post, format: :json)
