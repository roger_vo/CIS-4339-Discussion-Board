require 'acts-as-taggable-on'
class Post < ApplicationRecord
  acts_as_taggable
  acts_as_taggable_on :posts
  belongs_to :user, required: false
  belongs_to :channel, required: false
  has_many :comments

  include RailsSortable::Model
  set_sortable :sort
end
