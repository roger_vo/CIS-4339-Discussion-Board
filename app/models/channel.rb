class Channel < ApplicationRecord
  has_many :posts, inverse_of: :channel, dependent: :delete_all
  accepts_nested_attributes_for :posts, reject_if: :all_blank, allow_destroy: true
end
