# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(user_name: 'admin', password: 'Aa1Aa1', employee_no: '100001', email:'admin@admin.com',superadmin_role: 'TRUE') #1
User.create(user_name: 'supervisor', password: 'Aa1Aa1', employee_no: '100002', email:'supervisor@supervisor.com',supervisor_role: 'TRUE') #2
User.create(user_name: 'User', password: 'Aa1Aa1', employee_no: '100003', email:'user@user.com',user_role: 'TRUE') #3
User.create(user_name: 'cnguyen', password: 'Aa1Aa1', employee_no: '100004', email:'cnguyen@user.com',user_role: 'TRUE') #4
User.create(user_name: 'tnguyen', password: 'Aa1Aa1', employee_no: '100005', email:'tnguyen@user.com',user_role: 'TRUE') #5
User.create(user_name: 'rvo', password: 'Aa1Aa1', employee_no: '100006', email:'rvo@user.com',user_role: 'TRUE') #6
User.create(user_name: 'skalim', password: 'Aa1Aa1', employee_no: '100007', email:'skalim@user.com',user_role: 'TRUE') #7
User.create(user_name: 'skerai', password: 'Aa1Aa1', employee_no: '100008', email:'skerai@user.com',user_role: 'TRUE') #8

Channel.create(title: 'Announcement', description: 'Admin\'s announcements') #1
Channel.create(title: 'General Questions', description: 'Where we can share our day') #2
Channel.create(title: 'Marketing', description: 'Where we ask about marketing') #3
Channel.create(title: 'Sales', description: 'Where we want to know about sales') #4
Channel.create(title: 'Information Technology', description: 'Where we our computers are cured') #5
Channel.create(title: 'Human Resources\' announcement', description: 'Mostly where we are terminated ....') #6

Post.create(post_title: 'General Message', body: 'Use this board to post any questions you want answered', user_id: 1, channel_id: 1) #1
Post.create(post_title: 'Marketing Question', body: 'When are we going to market eastward?', user_id: 1, channel_id: 3) #2
Post.create(post_title: 'Sales cap?', body: 'Do we have a sales cap? If so how much?', user_id: 1, channel_id: 4) #3
Post.create(post_title: 'Operating system', body: 'Has anyone upgraded to the new Mac OS?', user_id: 1, channel_id: 5) #4

Post.create(post_title: 'Real patriotic question', body: 'When was America founded?', user_id: 2, channel_id: 2) #5
Post.create(post_title: 'HR!', body: 'Are we hiring? I am asking for a friend', user_id: 2, channel_id: 6) #6

Post.create(post_title: 'UNION', body: 'Who wants to form a mutiny against this company', user_id: 3, channel_id: 2) #7
Post.create(post_title: 'Wondering', body: 'When is the marketing client coming over', user_id: 3, channel_id: 3) #8
Post.create(post_title: 'URGENT', body: 'Where can report an incident', user_id: 3, channel_id: 6) #9

Post.create(post_title: '2nd Floor Fridge', body: 'Whoever is taking my food please stop. This is my food that I brought from home.', user_id: 4, channel_id: 2) #10
Post.create(post_title: 'Employee Benefits 2018', body: 'HR will be hosting a lunch and learn on Decemeber 18 to discuss the current employee benefits. Please come out!', user_id: 8, channel_id: 6) #11

Post.create(post_title: 'Phishing Email attacks', body: 'Report any suspicious emails to admin@admin.com', user_id: 1, channel_id: 1)
Post.create(post_title: 'Company Christmas Charity', body: 'Donate toys for children from now until December 24!', user_id: 1, channel_id: 1)
Post.create(post_title: 'All employees must use their badge to enter the doors', body: 'Please refrain from holding doors for others.', user_id: 1, channel_id: 1)
Post.create(post_title: 'Reminder to complete Employee Online Training', body: 'You must pass with an 80 to complete the training.', user_id: 1, channel_id: 1)
Post.create(post_title: 'Harvey Relief', body: 'Contact skalim and skerai if you have been affected by Harvey.', user_id: 1, channel_id: 1)

Comment.create(user_id: 2, body: 'I think in 2019', post_id: 2)
Comment.create(user_id: 2, body: 'Yeah we have one, its $100,000 ', post_id: 3)
Comment.create(user_id: 2, body: 'No I have not yet, anyone else?', post_id: 4)

Comment.create(user_id: 1, body: 'ME ME ME!', post_id: 7)
Comment.create(user_id: 3, body: 'I have and it sucks pls avoid.', post_id: 4)

Comment.create(user_id: 6, body: 'I have also noticed some of my food has also been missing this week.', post_id: 10)
Comment.create(user_id: 5, body: 'Sharing is caring!', post_id: 10)
