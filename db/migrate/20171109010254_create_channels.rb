class CreateChannels < ActiveRecord::Migration[5.1]
  def change
    create_table :channels do |t|
      t.string :title
      t.string :description
      t.boolean :isClosed, default: false
      t.boolean :isDeleted, default: false
      t.boolean :isPrivate, default: false

      t.timestamps
    end
  end
end
