class AddTagListToPosts < ActiveRecord::Migration[5.1]
  def change
    add_column :posts, :tag_list, :text
  end
end
