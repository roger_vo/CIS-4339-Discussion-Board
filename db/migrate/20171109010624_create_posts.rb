class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.string :post_title
      t.string :body
      t.references :user, foreign_key: true
      t.references :channel, foreign_key: true
      t.boolean :isDeleted, default: false
      t.boolean :isPrivate, default: false
      t.boolean :isEdited, default: false

      t.timestamps
    end
  end
end
