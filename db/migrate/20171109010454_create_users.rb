class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :user_name
      t.string :employee_no
      t.string :email
      t.string :phone_number
      t.string :address
      t.string :city
      t.string :state
      t.integer :zip_code
      t.boolean :isDeleted, default: false
      t.timestamps
    end
  end
end
