# README

# [Demo](https://discussion-board-project.herokuapp.com/)

NOTES ON NEW COMMTS: clear the database first and then run 

    rails db:migrate
    

Instructions on how to clone the master and push to new remote branch.

Based on what I searched: 

Step 1. you clone the master using this command 

    git clone <url> --branch <branch name> --single-branch <folder>

Ex: 

    git clone git@gitlab.keith-lancaster.com:cnnguy27/SemesterProject-DiscussionBoard.git --branch master --single-branch SemesterProject-DiscussionBoard
    
Result: if you do this command under the directory "/projects", for example, it will create a folder "/projects/SemesterProject-DiscussionBoard" with the files from the remote "master" branch
And when you do a "cd SemesterProject-DiscussionBoard" then "git branch" you will get the result like this "* master"
To rename your branch, cuz it is just cool to do so. you can do 

    git branch -m branchname new_branchname
    
or simply just 
    
    git branch -m Milky
    
because you are already in the branch master

Step 2. you do something with your local repository. 

Step 3. you do a combo 

Notes: when you clone, it automatically adds the remote for you to your local. You dont have to do "git add remote ....." The default remote name in your local machine after cloning is "origin"
    
    git add .  or git add <filename>
    git commit -m "<Your message>"
    git push origin <local_branch_name>:<remote_branch_name>
    
References: https://gist.github.com/hofmannsven/6814451